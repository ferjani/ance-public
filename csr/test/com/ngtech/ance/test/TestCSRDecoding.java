package com.ngtech.ance.test;

import com.ngtech.ance.CSRDecoder;
import com.ngtech.ance.beans.DecodedCSR;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Khadija Ferjani
 */
public class TestCSRDecoding
{

	private static final String csr2 = "-----BEGIN CERTIFICATE REQUEST-----\n"
		+ "MIIByjCCATMCAQAwgYkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9ybmlh\n"
		+ "MRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRMwEQYDVQQKEwpHb29nbGUgSW5jMR8w\n"
		+ "HQYDVQQLExZJbmZvcm1hdGlvbiBUZWNobm9sb2d5MRcwFQYDVQQDEw53d3cuZ29v\n"
		+ "Z2xlLmNvbTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEApZtYJCHJ4VpVXHfV\n"
		+ "IlstQTlO4qC03hjX+ZkPyvdYd1Q4+qbAeTwXmCUKYHThVRd5aXSqlPzyIBwieMZr\n"
		+ "WFlRQddZ1IzXAlVRDWwAo60KecqeAXnnUK+5fXoTI/UgWshre8tJ+x/TMHaQKR/J\n"
		+ "cIWPhqaQhsJuzZbvAdGA80BLxdMCAwEAAaAAMA0GCSqGSIb3DQEBBQUAA4GBAIhl\n"
		+ "4PvFq+e7ipARgI5ZM+GZx6mpCz44DTo0JkwfRDf+BtrsaC0q68eTf2XhYOsq4fkH\n"
		+ "Q0uA0aVog3f5iJxCa3Hp5gxbJQ6zV6kJ0TEsuaaOhEko9sdpCoPOnRBm2i/XRD2D\n"
		+ "6iNh8f8z0ShGsFqjDgFHyF3o+lUyj+UC6H1QW7bn\n" + "-----END CERTIFICATE REQUEST-----";

	private static final String csr3 = "-----BEGIN CERTIFICATE REQUEST-----\n"
		+ "MIICvDCCAaQCAQAwdzELMAkGA1UEBhMCVVMxDTALBgNVBAgMBFV0YWgxDzANBgNV\n"
		+ "BAcMBkxpbmRvbjEWMBQGA1UECgwNRGlnaUNlcnQgSW5jLjERMA8GA1UECwwIRGln\n"
		+ "aUNlcnQxHTAbBgNVBAMMFGV4YW1wbGUuZGlnaWNlcnQuY29tMIIBIjANBgkqhkiG\n"
		+ "9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8+To7d+2kPWeBv/orU3LVbJwDrSQbeKamCmo\n"
		+ "wp5bqDxIwV20zqRb7APUOKYoVEFFOEQs6T6gImnIolhbiH6m4zgZ/CPvWBOkZc+c\n"
		+ "1Po2EmvBz+AD5sBdT5kzGQA6NbWyZGldxRthNLOs1efOhdnWFuhI162qmcflgpiI\n"
		+ "WDuwq4C9f+YkeJhNn9dF5+owm8cOQmDrV8NNdiTqin8q3qYAHHJRW28glJUCZkTZ\n"
		+ "wIaSR6crBQ8TbYNE0dc+Caa3DOIkz1EOsHWzTx+n0zKfqcbgXi4DJx+C1bjptYPR\n"
		+ "BPZL8DAeWuA8ebudVT44yEp82G96/Ggcf7F33xMxe0yc+Xa6owIDAQABoAAwDQYJ\n"
		+ "KoZIhvcNAQEFBQADggEBAB0kcrFccSmFDmxox0Ne01UIqSsDqHgL+XmHTXJwre6D\n"
		+ "hJSZwbvEtOK0G3+dr4Fs11WuUNt5qcLsx5a8uk4G6AKHMzuhLsJ7XZjgmQXGECpY\n"
		+ "Q4mC3yT3ZoCGpIXbw+iP3lmEEXgaQL0Tx5LFl/okKbKYwIqNiyKWOMj7ZR/wxWg/\n"
		+ "ZDGRs55xuoeLDJ/ZRFf9bI+IaCUd1YrfYcHIl3G87Av+r49YVwqRDT0VDV7uLgqn\n"
		+ "29XI1PpVUNCPQGn9p/eX6Qo7vpDaPybRtA2R7XLKjQaF9oXWeCUqy1hvJac9QFO2\n"
		+ "97Ob1alpHPoZ7mWiEuJwjBPii6a9M9G30nUo39lBi1w=\n"
		+ "-----END CERTIFICATE REQUEST-----";

	private static final String csr1 = "-----BEGIN CERTIFICATE REQUEST-----\n"
		+ "MIICxDCCAawCAQAwfzELMAkGA1UEBhMCVVMxETAPBgNVBAgMCElsbGlub2lzMRAw\n"
		+ "DgYDVQQHDAdDaGljYWdvMQ4wDAYDVQQKDAVDb2RhbDELMAkGA1UECwwCTkExDjAM\n"
		+ "BgNVBAMMBUNvZGFsMR4wHAYJKoZIhvcNAQkBFg9rYmF4aUBjb2RhbC5jb20wggEi\n"
		+ "MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDSrEF27VvbGi5x7LnPk4hRigAW\n"
		+ "1feGeKOmRpHd4j/kUcJZLh59NHJHg5FMF7u9YdZgnMdULawFVezJMLSJYJcCAdRR\n"
		+ "hSN+skrQlB6f5wgdkbl6ZfNaMZn5NO1Ve76JppP4gl0rXHs2UkRJeb8lguOpJv9c\n"
		+ "tw+Sn6B13j8jF/m/OhIYI8fWhpBYvDXukgADTloCjOIsAvRonkIpWS4d014deKEe\n"
		+ "5rhYX67m3H7GtZ/KVtBKhg44ntvuT2fR/wB1FlDws+0gp4edlkDlDml1HXsf4FeC\n"
		+ "ogijo6+C9ewC2anpqp9o0CSXM6BT2I0h41PcQPZ4EtAc4ctKSlzTwaH0H9MbAgMB\n"
		+ "AAGgADANBgkqhkiG9w0BAQsFAAOCAQEAqfQbrxc6AtjymI3TjN2upSFJS57FqPSe\n"
		+ "h1YqvtC8pThm7MeufQmK9Zd+Lk2qnW1RyBxpvWe647bv5HiQaOkGZH+oYNxs1XvM\n"
		+ "y5huq+uFPT5StbxsAC9YPtvD28bTH7iXR1b/02AK2rEYT8a9/tCBCcTfaxMh5+fr\n"
		+ "maJtj+YPHisjxKW55cqGbotI19cuwRogJBf+ZVE/4hJ5w/xzvfdKjNxTcNr1EyBE\n"
		+ "8ueJil2Utd1EnVrWbmHQqnlAznLzC5CKCr1WfmnrDw0GjGg1U6YpjKBTc4MDBQ0T\n"
		+ "56ZL2yaton18kgeoWQVgcbK4MXp1kySvdWq0Bc3pmeWSM9lr/ZNwNQ==\n"
		+ "-----END CERTIFICATE REQUEST-----\n";
	
	private static CSRDecoder decoder;
	
	@BeforeClass
	public static void before()
	{
		decoder =  new CSRDecoder();
	}
	
	@Test
	public void testCSR1() throws IOException
	{
		DecodedCSR decodeCSR = decoder.decodeCSR(new ByteArrayInputStream(
			csr1.getBytes()));
		System.out.println("decoded csr 1: " + decodeCSR);
		Assert.assertEquals("country", "US", decodeCSR.getCountry());
		Assert.assertEquals("email", "kbaxi@codal.com", decodeCSR.getEmail());
		Assert.assertEquals("locality", "Chicago", decodeCSR.getLocality());
		Assert.assertEquals("organization", "Codal", decodeCSR.getOrganization());
		Assert.assertEquals("organization unit", "NA", decodeCSR.getOrganizationUnit());
		Assert.assertEquals("state", "Illinois", decodeCSR.getState());
		Assert.assertArrayEquals("fqdn", new String[] {"Codal"}, decodeCSR.getCNs());
	}
	
	@Test
	public void testCSR2() throws IOException
	{
		DecodedCSR decodeCSR = decoder.decodeCSR(new ByteArrayInputStream(
			csr2.getBytes()));
		System.out.println("decoded csr 2: " + decodeCSR);
		Assert.assertEquals("country", "US", decodeCSR.getCountry());
		Assert.assertNull("email", decodeCSR.getEmail());
		Assert.assertEquals("locality", "Mountain View", decodeCSR.getLocality());
		Assert.assertEquals("organization", "Google Inc", decodeCSR.getOrganization());
		Assert.assertEquals("organization unit", "Information Technology", decodeCSR.getOrganizationUnit());
		Assert.assertEquals("state", "California", decodeCSR.getState());
		Assert.assertArrayEquals("fqdn", new String[] {"www.google.com"}, decodeCSR.getCNs());
	}
	
	@Test	
	public void testCSR3() throws IOException
	{
		DecodedCSR decodeCSR = decoder.decodeCSR(new ByteArrayInputStream(
			csr3.getBytes()));
		System.out.println("decoded csr 3: " + decodeCSR);
		Assert.assertEquals("country", "US", decodeCSR.getCountry());
		Assert.assertNull("email", decodeCSR.getEmail());
		Assert.assertEquals("locality", "Lindon", decodeCSR.getLocality());
		Assert.assertEquals("organization", "DigiCert Inc.", decodeCSR.getOrganization());
		Assert.assertEquals("organization unit", "DigiCert", decodeCSR.getOrganizationUnit());
		Assert.assertEquals("state", "Utah", decodeCSR.getState());
		Assert.assertArrayEquals("fqdn", new String[] {"example.digicert.com"}, decodeCSR.getCNs());
	}


	
}
