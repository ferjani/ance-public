package com.ngtech.ance.test;

import com.ngtech.ance.SSLCSRDecoder;
import com.ngtech.ance.beans.DecodedSSLCSR;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Khadija Ferjani
 */
public class TestSSLDecoding
{

	private static final String csr4 = "-----BEGIN CERTIFICATE REQUEST-----\n"
		+ "MIIE1zCCAr8CAQAwTzELMAkGA1UEBhMCVE4xEDAOBgNVBAgMB0VuZ2xhbmQxETAP\n"
		+ "BgNVBAcMCEJyaWdodG9uMQwwCgYDVQQKDANHRkkxDTALBgNVBAMMBFRFU1QwggIi\n"
		+ "MA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC8eE/ZPF+2km22lxt9GOML7IZH\n"
		+ "bB1T5CCHflS/H802hU3faJszHCxm2arn4yYrimvzQRm+70SyVD5wKghBU1MH1ysf\n"
		+ "D9o0Jp+uS+I9hx5ZSceKQ3ccI7s+9uFsLw2zIapKrbJzG0tdKpL5IRs4lC/TWF5G\n"
		+ "HQbDNDkM1/u9AU8U8mLYAcp5ru+sw3VkZFWxHFcmryf5mU6gNbt9fgt8AExIdFdw\n"
		+ "jfuhCxTEsXqf9dyGL0oYk37pUTGYtZM3gcTUDTec532yed1z2KIP8O6BGKQFQzSq\n"
		+ "0uwKer50Vj3PTm33mcKKAWJDO4lObDhRMx15t8Ui2P5oIrTf+NvBnPp6wuPEJfHK\n"
		+ "vp8gGP1wRw/xLcpsIH+YIaDMIqOH2TsuB7wJPbXWjkfygYTsTo5P2rOLLiyZMApk\n"
		+ "Us7VPmZiWmimdJj0D5JlfKbOg+xMfgbXPsSin9uXg2yY6Btd+wgulCHKUbrbteUd\n"
		+ "LrsDPTDGcdS60zplt0NmVDQKSpHW9AZCgilUnCi2wNhWBJk0kyRaeM8NVPvxBUug\n"
		+ "BMkSqFgxN+4sWBaKyRmf13PoPen2UUZ+UfhC596IMQLaMYcFVKHJWfMd7+TdSpmk\n"
		+ "KYXtK7AoiFIAmfAcWajTjmXxdKXQUHQ4IKyd26jopT6Fq+LmOWnL7pjN9MPTwRA8\n"
		+ "UXevJcpXYYT1bw8r0QIDAQABoEMwQQYJKoZIhvcNAQkOMTQwMjAwBgNVHREEKTAn\n"
		+ "ghB5b3VyLXdlYnNpdGUuZGV2ghNhbm90aGVyLXdlYnNpdGUuZGV2MA0GCSqGSIb3\n"
		+ "DQEBCwUAA4ICAQCFNNkkNJQR5tn3h50k+dL5RTOSqzukxWwKx6fmQkUT8sNmCpmw\n"
		+ "ALXTu0HMTg7mQ4yalhnmIzYU8w4oMzxlrUAyP4iJwku+lo07PJtqnKxuGD7CkyFP\n"
		+ "6sOad6IhecQGYJWDX3hoF1clRyGVN1Vpfzy8SgUjpaFWmF852PxSRB844Go+KryB\n"
		+ "ryBLV1WNE+wDglRwc+QftDEWpPuAMdOZqNd8DVEhdD1kZfcjPAWiA02P5Tdjy9yq\n"
		+ "1NgmxNq+S9IWALe+HyS4z4HckSb13piMEl8PsfpWMJeTc9rSee2P6lyr6nLJt4Me\n"
		+ "innY7Th6lpIRjcr1Blo3qrsPz7PZOhwKRn0g9peqSXlhRvlU0oZMoSXpc3vtHUGZ\n"
		+ "OVNEzkbXa8oA+1dt8v6Q76AiHok4twmCGxLKhtCHFciOqnhF1MGBS59iVwB7MktH\n"
		+ "62aURWx5ru/JFc3FXq6UjY2B9RNJ3dcCkLn4tQllyTCJ/3iaQx4l/PLA8sg7a8H/\n"
		+ "sSsTN1cLv87A4Ce4f6k+WSfHQLH59JqHiokburtBW4nj6D3pHRSOBKvUOrgDa/U4\n"
		+ "IobLbk2/bGiYy0fQKHYAXN3ImlT5XFS6WJsRqZRaq4nzP7JigmqHeeDHpJ26xHX1\n"
		+ "wQAsq0m2MV2XlOE5HsQsW2Jt3Mu2rwoyfT8LODktA8MByQSAndMhG+VRnw==\n"
		+ "-----END CERTIFICATE REQUEST-----";

	private static String csr5 = "-----BEGIN CERTIFICATE REQUEST-----\n"
		+ "MIIBzDCCATUCAQAwPTEcMBoGCSqGSIb3DQEJARYNdGVzdEB0ZXN0LmNvbTELMAkG\n"
		+ "A1UEBhMCVE4xEDAOBgNVBAoTB05HIFRlY2gwgZ8wDQYJKoZIhvcNAQEBBQADgY0A\n"
		+ "MIGJAoGBAIqmqhvXiHX3B/NjdGucMhI225IRPkWfvwC+qZwUHbGhposrvjCJYaGH\n"
		+ "2H+pvhdtBW8ouW2F4HIN85J0Vgq+0685J875pS35CBRgcY45QMqBXa9utgFeFBbw\n"
		+ "paJq94P7sXe0bsp7L1GE8GbXusb0mo8K5w6r6pSCttZVab5u0GAVAgMBAAGgTzBN\n"
		+ "BgkqhkiG9w0BCQ4xQDA+MDwGA1UdEQQ1MDOCD3N1YjEuZG9tYWluLmNvbYIPc3Vi\n"
		+ "Mi5kb21haW4uY29tgg9zdWIzLmRvbWFpbi5jb20wDQYJKoZIhvcNAQELBQADgYEA\n"
		+ "eT8jAk1NLvitsiblN099/YWHPELJ54xAavy5a9mPvbQ7n498XxptMdrPS6aJtK3e\n"
		+ "Ca4Zm6o0badX4VUFRCgCEQ0GSxxd0h4MR8IREHRdvRL3Inr7U8SSvO6SyHf28KiH\n"
		+ "MeztAkqVVzH4xlrg8RMsVeF3PwFwmQWtEQ5nqXWaV1s=\n"
		+ "-----END CERTIFICATE REQUEST-----";

	private static String csr6 = "-----BEGIN CERTIFICATE REQUEST-----\n"
		+ "MIICtjCCAZ4CAQAwcTELMAkGA1UEBhMCVE4xGDAWBgNVBAMMD3d3dy5uZy1zaWdu\n"
		+ "LmNvbTENMAsGA1UEBwwEVGVzdDEYMBYGA1UECgwPTkcgVGVjaG5vbG9naWVzMRIw\n"
		+ "EAYDVQQIDAlCZW4gQXJvdXMxCzAJBgNVBAsMAklUMIIBIjANBgkqhkiG9w0BAQEF\n"
		+ "AAOCAQ8AMIIBCgKCAQEA5G/0/gQdl9IlMxZxGklIriY+1W+FSQgNFAb9OH8Sbb1X\n"
		+ "dQ/3LXds3uOldJOdXQcTD8OjERVBOdurVg93XK4TMjRsJxOzl1AMszCDgajb8JzS\n"
		+ "VfJTvKJgIyPNDMao9gqzH1oeLaB5Ma2ymdkuz2Guc1AFz60oat3IzNP5tisfMmUq\n"
		+ "kAlEXIg4RcuQIGhF4ynLMkEDr30tEVLKWWyF2fk1Y1+bLYetPZFaUW94gUSAihWR\n"
		+ "RMu41ga7gk/lAOL8AZGpa3RepucFlLI2Ej2BB1f6j9cPLzFPS5K+6S9i79lXDvl3\n"
		+ "by1PxzN499keKGfU0J9H82oD0uqu/40bzZ/EJUjkWQIDAQABoAAwDQYJKoZIhvcN\n"
		+ "AQELBQADggEBAHi5+rMbco2t5tiOnMYM/ipT/ZCh7MUqe/DJCxxo42u6UfopH8Eu\n"
		+ "Ntoy/4P1QpBGiVlpeKgvH5a7w/JbYtTnjzy1PnJMe0nmvs6AjKbqXfqC1iVGFk3j\n"
		+ "hy7culA+KnvE3wspIjy17xSeAJitsWwqYBAQ43r5tY/YIQb8JOfC+i7ZFr1F9i0N\n"
		+ "XXkJDzah5UNWxF2Zk9+YsocSQOAIQZBJ/Yf/8zKYTtBajgVYYZ1tkK31dm7dP9HB\n"
		+ "IiTNMU/JK//suO51UvRsnRgE3Vuh6DrxPd0fG9VwgukJkJsPTpgXzeZi78RG4l2E\n"
		+ "GOM3eLTwolgzQWQKVBfKP8GoAidnFS7nNHQ=\n" + "-----END CERTIFICATE REQUEST-----";

	private static String csr7 = "-----BEGIN CERTIFICATE REQUEST-----\n"
		+ "MIIC6zCCAdMCAQAwVzEcMBoGCSqGSIb3DQEJARYNdGVzdEB0ZXN0LmNvbTELMAkG\n"
		+ "A1UEBhMCVE4xEDAOBgNVBAoTB05HIFRlY2gxGDAWBgNVBAMTD3N1YjEuZG9tYWlu\n"
		+ "LmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAINCGWVTI3PyxO2m\n"
		+ "z4FtsZVbzZwr8Z/5k+oISOOjmaOQc+ljFNibohdGigTfNjVqHuQUuqRqWbceSiIS\n"
		+ "0HF7CZnuuD4BEbDw+lI+SwTw1G9SXTi18rAc8Oyge77v6uzRsnQcaEX2Hw4bFmzy\n"
		+ "4QleNI1uxG8Hl9KHgvpTzm4OqBf2gS33E0Z+NSSL55Y7psWKN/nU5XyQkuAshS9M\n"
		+ "GKHvNAaBFm2ENhSLTafNR3q4fRWHdFoWGMe/oYmUPbETHnDHXrz37lONGz3IrWJz\n"
		+ "CgF2jt+WGI3RLqS8g+zL6eqdhXklUlnYTYlO+10nO1E1UxWmw5njLw5ITuldqnBQ\n"
		+ "zvZf2w8CAwEAAaBPME0GCSqGSIb3DQEJDjFAMD4wPAYDVR0RBDUwM4IPc3ViMS5k\n"
		+ "b21haW4uY29tgg9zdWIyLmRvbWFpbi5jb22CD3N1YjMuZG9tYWluLmNvbTANBgkq\n"
		+ "hkiG9w0BAQsFAAOCAQEAJwwKVgH/0lKt1A7Pod0TppehuZAof97mR3ydwO4ULdFI\n"
		+ "if4en1pUuTPzqOfKB3OkZXc0D8PX+VUOpTQ9KGtlE9pSedDXTB57IN8nWwFu6ibN\n"
		+ "zG13wy5K+7F/1XxROwM1NemHOtGR+w38fuV8IMYqSkhwr6MPE3/wxPvQ8n2jNr9Z\n"
		+ "sZ+JlMv9Xd8mlzfAvrxNg73lQUMW6bMTeIXH3bo5Scf4mI0cMQA6pLR9oM9gsBPQ\n"
		+ "IrnoPFdYk+24SbbHnShXLPcrbqa2EfBcLWVpnf8zthE7+bhNbpEoTK2FRBScOjmw\n"
		+ "S0tBlIWYofsYifBRNHxwlY6iuUHcKk6/sFZSnDn6DQ==\n" +
	"-----END CERTIFICATE REQUEST-----";
	private static SSLCSRDecoder decoder;

	@BeforeClass
	public static void before()
	{
		decoder = new SSLCSRDecoder();
	}

	@Test
	public void testCSR4() throws IOException
	{
		DecodedSSLCSR decodeCSR = decoder.decodeCSR(new ByteArrayInputStream(
			csr4.getBytes()));
		System.out.println("decoded csr 4: " + decodeCSR);
		Assert.assertEquals("country", "TN", decodeCSR.getCountry());
		Assert.assertNull("email", decodeCSR.getEmail());
		Assert.assertArrayEquals("CN", new String[] {"TEST"}, decodeCSR.getCn());
		Assert.assertEquals("organization", "GFI", decodeCSR.getOrganization());
		Assert.assertArrayEquals("fqdn", new String[]{"your-website.dev",
			"another-website.dev"}, decodeCSR.getFqdn());
		Assert.assertEquals("SHA256WITHRSA", decodeCSR.getAlgorithm());
		Assert.assertEquals(4096, decodeCSR.getSize());
	}

	@Test
	public void testCSR5() throws IOException
	{
		DecodedSSLCSR decodeCSR = decoder.decodeCSR(new ByteArrayInputStream(
			csr5.getBytes()));
		System.out.println("decoded csr 4: " + decodeCSR);
		Assert.assertEquals("country", "TN", decodeCSR.getCountry());
		Assert.assertEquals("test@test.com", decodeCSR.getEmail());
		Assert.assertArrayEquals("CN", new String[] {}, decodeCSR.getCn());
		Assert.assertEquals("organization", "NG Tech", decodeCSR.getOrganization());
		Assert.assertArrayEquals("fqdn", new String[]{"sub1.domain.com",
			"sub2.domain.com", "sub3.domain.com"}, decodeCSR.getFqdn());
		Assert.assertEquals("SHA256WITHRSA", decodeCSR.getAlgorithm());
		Assert.assertEquals(1024, decodeCSR.getSize());
	}

	@Test
	public void testCSR6() throws IOException
	{
		DecodedSSLCSR decodeCSR = decoder.decodeCSR(new ByteArrayInputStream(
			csr6.getBytes()));
		System.out.println("decoded csr 6: " + decodeCSR);
		Assert.assertEquals("country", "TN", decodeCSR.getCountry());
		Assert.assertEquals("CN", "www.ng-sign.com", decodeCSR.getCn()[0]);
		Assert.assertNull(decodeCSR.getEmail());
		Assert.assertEquals("organization", "NG Technologies", decodeCSR.getOrganization());
		Assert.assertArrayEquals("fqdn", new String[]{}, decodeCSR.getFqdn());
		Assert.assertEquals("SHA256WITHRSA", decodeCSR.getAlgorithm());
		Assert.assertEquals(2048, decodeCSR.getSize());
	}

	@Test
	public void testCSR7() throws IOException
	{
		DecodedSSLCSR decodeCSR = decoder.decodeCSR(new ByteArrayInputStream(
			csr7.getBytes()));
		System.out.println("decoded csr 7: " + decodeCSR);
		Assert.assertEquals("country", "TN", decodeCSR.getCountry());
		Assert.assertEquals("CN", "sub1.domain.com", decodeCSR.getCn()[0]);
		Assert.assertNotNull(decodeCSR.getEmail());
		Assert.assertEquals("organization", "NG Tech", decodeCSR.getOrganization());
		Assert.assertArrayEquals("fqdn", new String[]{"sub1.domain.com", "sub2.domain.com", "sub3.domain.com"}, decodeCSR.getFqdn());
		Assert.assertEquals("SHA256WITHRSA", decodeCSR.getAlgorithm());
		Assert.assertEquals(2048, decodeCSR.getSize());
	}
}
