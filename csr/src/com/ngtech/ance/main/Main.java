package com.ngtech.ance.main;

import java.io.IOException;
import java.io.StringWriter;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.ExtensionsGenerator;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.bouncycastle.util.io.pem.PemObject;

/**
 * This is a helpful class to generate SSL certificates with the subject alternative names
 * extension.
 *
 * @author Khadija Ferjani
 */
public class Main
{

	private static String org = "NG Tech";
	private static String c = "TN";
	private static String email = "test@test.com";
	private static String[] fqdns = {"sub1.domain.com", "sub2.domain.com", "sub3.domain.com"};
	private static boolean withCN = true;

	/**
	 * Main method to generate SSL CSRs.
	 *
	 * @param args input argument
	 * @throws OperatorCreationException on key pair generation error
	 * @throws NoSuchAlgorithmException if the algorithm is unknown
	 * @throws IOException on I/O errors
	 */
	public static void main(String... args) throws OperatorCreationException,
		NoSuchAlgorithmException,
		IOException
	{
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");

		KeyPair pair = kpg.generateKeyPair();
		String dn;
		if (withCN) {
			dn = "CN=" + fqdns[0] + ",O=" + org + ", C=" + c
				+ ", EMAILADDRESS=" + email;
		} else {
			dn = "O=" + org + ", C=" + c + ", EMAILADDRESS=" + email;
		}
		PKCS10CertificationRequestBuilder p10Builder
			= new JcaPKCS10CertificationRequestBuilder(new X500Principal(dn),
				pair.getPublic());

		ExtensionsGenerator extensionsGenerator = new ExtensionsGenerator();

		List<GeneralName> subjectAltNames = new ArrayList();
		for (String fqdn : fqdns) {
			subjectAltNames.add(new GeneralName(GeneralName.dNSName, fqdn));
		}
		GeneralNames subjectAltName = new GeneralNames(subjectAltNames.toArray(
			new GeneralName[0]));
		extensionsGenerator.addExtension(Extension.subjectAlternativeName, false,
			subjectAltName.toASN1Primitive());

		p10Builder.addAttribute(PKCSObjectIdentifiers.pkcs_9_at_extensionRequest,
			extensionsGenerator.generate());

		JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA256withRSA");

		ContentSigner signer = csBuilder.build(pair.getPrivate());

		PKCS10CertificationRequest csr = p10Builder.build(signer);

		PemObject pemObject = new PemObject("CERTIFICATE REQUEST", csr.getEncoded());
		StringWriter str = new StringWriter();
		PEMWriter pemWriter = new PEMWriter(str);
		pemWriter.writeObject(pemObject);
		pemWriter.close();
		str.close();
		System.out.println(str);

	}
}
