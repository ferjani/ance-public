package com.ngtech.ance;

import com.ngtech.ance.beans.DecodedSSLCSR;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DLSequence;
import org.bouncycastle.asn1.pkcs.Attribute;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.operator.DefaultAlgorithmNameFinder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

/**
 * This class decodes a PEM encoded SSL CSR and returns the most important (known) information.
 *
 * @author Khadija Ferjani
 */
public class SSLCSRDecoder extends AbstractDecoder
{

	/**
	 * Decodes the SSL CSR.
	 *
	 * @param csrStream the binary PEM encoded stream
	 * @return the decoded CSR
	 * @throws IOException on I/O errors
	 */
	public DecodedSSLCSR decodeCSR(ByteArrayInputStream csrStream)
		throws IOException
	{

		PKCS10CertificationRequest csr = pemToPKCS10(csrStream);

		X500Name x500Name = csr.getSubject();
		String e = getX500Field(BCStyle.E.getId(), x500Name);
		String c = getX500Field(BCStyle.C.getId(), x500Name);
		String o = getX500Field(BCStyle.O.getId(), x500Name);
		String[] cn = getX500Fields(BCStyle.CN.getId(), x500Name);

		Extensions exts = getExtensionsFromCSR(csr);

		String[] fqdn = {};
		if (exts != null) {
			GeneralNames gns = GeneralNames.fromExtensions(
				exts, Extension.subjectAlternativeName);
			if (gns != null) {
				fqdn = new String[gns.getNames().length];
				for (int i = 0; i < fqdn.length; i++) {
					fqdn[i] = gns.getNames()[i].getName().toString();
				}
			}
		}
		DefaultAlgorithmNameFinder f = new DefaultAlgorithmNameFinder();

		return new DecodedSSLCSR(cn, fqdn, c, e, o,
			f.getAlgorithmName(csr.getSignatureAlgorithm()),
			csr.getSignature().length * 8);
	}

	/**
	 * Gets the extensions from the decoded CSR file. May return {@code null} if the CSR
	 * does not have any extensions
	 * 
	 * @param csr the decoded CSR
	 * @return the extensions
	 */
	public static Extensions getExtensionsFromCSR(PKCS10CertificationRequest csr)
	{
		Attribute[] attributess = csr.getAttributes(
			PKCSObjectIdentifiers.pkcs_9_at_extensionRequest);
		for (Attribute attribute : attributess) {
			ASN1Set attValue = attribute.getAttrValues();
			if (attValue != null) {
				ASN1Encodable extension = attValue.getObjectAt(0);
				if (extension instanceof Extensions) {
					return (Extensions) extension;
				} else if (extension instanceof DERSequence
					|| extension instanceof DLSequence) {
					return Extensions.getInstance(extension);
				}
			}
		}
		return null;
	}
}
