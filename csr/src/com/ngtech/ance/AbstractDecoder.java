package com.ngtech.ance;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.Security;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

/**
 * This is an abstract class that is used for code factorization.
 *
 * @author Khadija Ferjani
 */
class AbstractDecoder
{

	/**
	 * Decodes the given input stream.
	 * 
	 * @param pem the PEM encoded stream
	 * @return the decoded CSR
	 * @throws IOException on I/O errors
	 */
	static PKCS10CertificationRequest pemToPKCS10(ByteArrayInputStream pem)
		throws IOException
	{
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		PKCS10CertificationRequest csr = null;

		Reader pemReader = new BufferedReader(new InputStreamReader(pem));
		try (PEMParser pemParser = new PEMParser(pemReader)) {
			Object parsedObj = pemParser.readObject();
			if (parsedObj instanceof PKCS10CertificationRequest) {
				csr = (PKCS10CertificationRequest) parsedObj;
			}
		}

		return csr;
	}

	/**
	 * Gets a single field from the decoded CSR.
	 * 
	 * @param asn1ObjectIdentifier the field identifier
	 * @param x500Name the X500 name to filter
	 * @return the selected field
	 */
	String getX500Field(String asn1ObjectIdentifier, X500Name x500Name)
	{
		RDN[] rdnArray = x500Name.getRDNs(new ASN1ObjectIdentifier(asn1ObjectIdentifier));

		String retVal = null;
		for (RDN item : rdnArray) {
			retVal = item.getFirst().getValue().toString();
		}
		return retVal;
	}

	/**
	 * Gets an array of fields from the decoded CSR.
	 * 
	 * @param asn1ObjectIdentifier the field identifier
	 * @param x500Name the X500 name to filter
	 * @return the selected fields
	 */
	String[] getX500Fields(String asn1ObjectIdentifier, X500Name x500Name)
	{
		RDN[] rdnArray = x500Name.getRDNs(new ASN1ObjectIdentifier(asn1ObjectIdentifier));

		String[] retVal = new String[rdnArray.length];

		for (int i = 0; i < rdnArray.length; i++) {
			retVal[i] = rdnArray[i].getFirst().getValue().toString();
		}
		return retVal;
	}
}
