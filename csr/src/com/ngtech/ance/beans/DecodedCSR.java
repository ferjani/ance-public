package com.ngtech.ance.beans;

import java.util.Arrays;

/**
 * This bean represents a decoded CSR. It contains the main information that are needed to verify a
 * CEV or Enterprise IT certificate request.
 *
 * <br>
 * <b>All returned information but the common name may be {@code null}.</b>
 *
 * @author Khadija Ferjani
 */
public class DecodedCSR extends AbstractDecodedCSR
{

	private final String locality;
	private final String organizationUnit;
	private final String state;
	private final String[] cns;

	/**
	 * Creates a decoded CSR bean.
	 *
	 * @param country the requested country
	 * @param email the request email
	 * @param locality the locality
	 * @param organization the organization
	 * @param organizationUnit the organization unit
	 * @param state the state
	 * @param cn the list of common names
	 * @param algorithm the signing algorithm
	 * @param size the key pair size
	 */
	public DecodedCSR(String[] cn, String country, String email, String locality,
		String organization,
		String organizationUnit, String state, String algorithm, int size)
	{
		super(cn, country, email, organization, algorithm, size);
		this.locality = locality;
		this.organizationUnit = organizationUnit;
		this.state = state;
		this.cns = cn;
	}

	/**
	 * Returns the locality.
	 * 
	 * @return the locality.
	 */
	public String getLocality()
	{
		return locality;
	}

	/**
	 * Returns the organization unit.
	 * 
	 * @return the organization unit.
	 */
	public String getOrganizationUnit()
	{
		return organizationUnit;
	}

	/**
	 * Returns the state.
	 * 
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * Returns the common names.
	 * 
	 * @return the common names.
	 */
	public String[] getCNs()
	{
		return cns;
	}

	@Override
	public String toString()
	{
		return super.toString() + ", locality=" + locality + ", organizationUnit="
			+ organizationUnit + ", state=" + state + ", CNs="
			+ Arrays.toString(cns);
	}

}
