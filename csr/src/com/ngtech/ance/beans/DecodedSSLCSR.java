package com.ngtech.ance.beans;

import java.util.Arrays;

/**
 * This bean represents a decoded CSR. It contains the main information that are needed to verify an
 * SSL certificate request.
 *
 * <br>
 * <b>Any information other than the FQDN may be {@code null}.</b>
 * A malformed CSR may have the FQDN list empty.
 *
 * @author Khadija Ferjani
 */
public class DecodedSSLCSR extends AbstractDecodedCSR
{

	private final String[] fqdn;

	/**
	 * Creates a decoded SSL csr.
	 * 
	 * @param cn the list of common names
	 * @param country the country
	 * @param email the email
	 * @param organization the organization
	 * @param algorithm the signature algorithm
	 * @param size the key pair size
	 * @param fqdn the FQDN
	 */
	public DecodedSSLCSR(String[] cn, String[] fqdn,
		String country, String email, String organization, String algorithm, int size)
	{
		super(cn, country, email, organization, algorithm, size);
		this.fqdn = fqdn;
	}

	/**
	 * Returns the list of FQDNs.
	 * 
	 * @return the list of FQDN.
	 */
	public String[] getFqdn()
	{
		return fqdn;
	}

	@Override
	public String toString()
	{
		return super.toString() + ", fqdn: " + Arrays.toString(fqdn);
	}

}
