package com.ngtech.ance.beans;

/**
 * Abstract bean.
 *
 * @author Khadija Ferjani
 */
abstract class AbstractDecodedCSR
{
	private final String[] cn;
	private final String country;
	private final String email;
	private final String organization;
	private final String algorithm;
	private final int size;

	/**
	 * Creates an abstract decoded CSR.
	 * 
	 * @param cn the list of common names
	 * @param country the country
	 * @param email the email
	 * @param organization the organization
	 * @param algorithm the signature algorithm
	 * @param size the key pair size
	 */
	public AbstractDecodedCSR(String[] cn, String country, String email, String organization,
		String algorithm, int size)
	{
		this.country = country;
		this.email = email;
		this.organization = organization;
		this.algorithm = algorithm;
		this.size = size;
		this.cn = cn;
	}

	/**
	 * Returns the country name.
	 * 
	 * @return the country name
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * Returns the email.
	 * 
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * Returns the organization.
	 * 
	 * @return the organization
	 */
	public String getOrganization()
	{
		return organization;
	}
	/**
	 * Returns the signature algorithm.
	 * 
	 * @return the signature algorithm
	 */
	public String getAlgorithm()
	{
		return algorithm;
	}

	/**
	 * Returns the key pair size.
	 * 
	 * @return the key pair size
	 */
	public int getSize()
	{
		return size;
	}

	/**
	 * Return the array of common names.
	 * 
	 * @return the common names.
	 */
	public String[] getCn()
	{
		return cn;
	}

	@Override
	public String toString()
	{
		return "country=" + country + ", email="
			+ email + ", organizaiton=" + organization;
	}

}
