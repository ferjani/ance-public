package com.ngtech.ance;

import com.ngtech.ance.beans.DecodedCSR;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.operator.DefaultAlgorithmNameFinder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

/**
 * This class allows to decode CEV and Enterprise-ID CSRs.
 *
 * @author Khadija Ferjani
 */
public class CSRDecoder extends AbstractDecoder
{

	/**
	 * Decodes the CSR (CEV, Enterprise ID, ...).
	 *
	 * @param csrStream the binary PEM encoded stream
	 * @return the decoded CSR
	 * @throws IOException on I/O errors
	 */
	public DecodedCSR decodeCSR(ByteArrayInputStream csrStream)
		throws IOException
	{

		PKCS10CertificationRequest csr = pemToPKCS10(csrStream);

		X500Name x500Name = csr.getSubject();
		String e = getX500Field(BCStyle.E.getId(), x500Name);
		String c = getX500Field(BCStyle.C.getId(), x500Name);
		String l = getX500Field(BCStyle.L.getId(), x500Name);
		String o = getX500Field(BCStyle.O.getId(), x500Name);
		String ou = getX500Field(BCStyle.OU.getId(), x500Name);
		String s = getX500Field(BCStyle.ST.getId(), x500Name);
		String[] cn = getX500Fields(BCStyle.CN.getId(), x500Name);

		DefaultAlgorithmNameFinder f = new DefaultAlgorithmNameFinder();
		return new DecodedCSR(cn, c, e, l, o, ou, s,
			f.getAlgorithmName(csr.getSignatureAlgorithm()),
			csr.getSignature().length * 8);
	}

}
