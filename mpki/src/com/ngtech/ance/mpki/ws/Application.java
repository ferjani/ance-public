package com.ngtech.ance.mpki.ws;

import javax.security.auth.message.config.AuthConfigFactory;

import org.apache.catalina.authenticator.jaspic.AuthConfigFactoryImpl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * This class is the entry point to launch a server.
 *
 * @author Khadija Ferjani
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.ngtech.ance.mpki.ws", "com.ngtech.ance.mpki.ws.services"})
@EnableJpaRepositories(basePackages = {"com.ngtech.ance.mpki.ws.repositories"})
@EntityScan("com.ngtech.ance.mpki.ws.entities")
@EnableScheduling
public class Application
{

	/**
	 * Entry point to launch a server.
	 *
	 * @param args optional arguments
	 */
	public static void main(String[] args)
	{
		if (AuthConfigFactory.getFactory() == null) {
			AuthConfigFactory.
				setFactory(new AuthConfigFactoryImpl());
		}
		SpringApplication.run(Application.class, args);
	}
}
