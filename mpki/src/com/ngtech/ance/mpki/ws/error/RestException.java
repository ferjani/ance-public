package com.ngtech.ance.mpki.ws.error;

/**
 * Exception relative to Registration Authority server.
 *
 * @author Aroua Souabni
 */
public class RestException extends Exception
{

	private final int errorCode;

	/**
	 * Constructor with parameters.
	 *
	 * @param errorCode error code
	 * @param errorMessage error message
	 */
	public RestException(int errorCode, String errorMessage)
	{
		super(errorMessage);
		this.errorCode = errorCode;
	}

	/**
	 * Gets error code.
	 *
	 * @return error code
	 */
	public int getErrorCode()
	{
		return errorCode;
	}

}
