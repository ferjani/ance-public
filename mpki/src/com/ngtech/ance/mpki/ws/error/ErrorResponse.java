package com.ngtech.ance.mpki.ws.error;

/**
 * This bean is returned by the web services in case of error.
 *
 * @author Khadija Ferjani
 */
public class ErrorResponse
{

	private int errorCode;
	private String message;

	/**
	 * Returns the error code.
	 *
	 * @return the error code
	 */
	public int getErrorCode()
	{
		return errorCode;
	}

	/**
	 * Returns the error code.
	 *
	 * @param errorCode the error code
	 */
	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}

	/**
	 * Returns the error message.
	 *
	 * @return the detailed error message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * Sets the error message.
	 *
	 * @param message the detailed error message
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}
}
