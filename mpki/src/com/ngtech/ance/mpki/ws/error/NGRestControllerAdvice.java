package com.ngtech.ance.mpki.ws.error;

import java.io.IOException;
import java.net.ConnectException;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

/**
 * A controller advice is a global exception handler. It handles the declared
 * exceptions in a unique way through the whole application. This is a kind of
 * "annotation driven interceptor". <br>
 *
 * Technically, this class is a {@link Component} that is auto detected by
 * classpath scanning. It is used to define @ExceptionHandler using the
 * implementation
 * {@link org.springframework.web.servlet.mvc.method.annotation.
 * ExceptionHandlerExceptionResolver}
 * 
 * From spring documentation: <br>
 * The @ControllerAdvice annotation is a component annotation allowing
 * implementation classes to be auto-detected through classpath scanning.
 *
 * <b>Note:</b> This handler only assists Rest controllers.
 *
 * @author Khadija FERJANI
 */
@RestControllerAdvice(annotations = {RestController.class})
public class NGRestControllerAdvice
{

	private static final Logger LOGGER = LoggerFactory.getLogger(
		NGRestControllerAdvice.class);

	/**
	 * Handles {@link RestException} exceptions.
	 *
	 * @param request the failing request
	 * @param ex the REST exception
	 * @return a properly configured error response
	 */
	@ExceptionHandler(RestException.class)
	public ResponseEntity<ErrorResponse> handleError(
		HttpServletRequest request, RestException ex)
	{
		ErrorResponse error = new ErrorResponse();
		error.setMessage(ex.getMessage());
		error.setErrorCode(ex.getErrorCode());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Handles {@link IllegalArgumentException} exceptions. This exception 
	 * is typically throws
	 * when the client input are not valid (calls to {@code Assert.* fails}
	 *
	 * @param request the failing request
	 * @param ex instance of {@link IllegalArgumentException}
	 * @return the response with the HTTPStatus BAD_REQUEST (400) and the
	 * corresponding HTTPBody (The message from the thrown exception)
	 */
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<ErrorResponse> handleError(
		HttpServletRequest request, IllegalArgumentException ex)
	{
		ErrorResponse error = new ErrorResponse();
		error.setMessage(ex.getLocalizedMessage());
		error.setErrorCode(HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	/**
	 *  Handles {@link HttpClientErrorException} exceptions.
	 * @param ex the failing request
	 * @return instance of {@link HttpClientErrorException}
	 * @throws IOException on I/O errors
	 */
	@ExceptionHandler(HttpClientErrorException.class)
	public ResponseEntity<ErrorResponse> handleError(
		HttpClientErrorException ex)
		throws IOException
	{
		String response = ex.getResponseBodyAsString();
		ObjectMapper mapper = new ObjectMapper();
		ErrorResponse error = mapper.readValue(response,
			ErrorResponse.class);

		return new ResponseEntity<>(error, ex.getStatusCode());
	}

	/**
	 *  Handles {@link HttpServerErrorException} exceptions.
	 * @param request ex the failing request
	 * @param ex instance of {@link HttpServerErrorException}
	 * @return the response with the HTTPStatus BAD_REQUEST (400) and the
	 * corresponding HTTPBody (The message from the thrown exception)
	 */
	@ExceptionHandler(HttpServerErrorException.class)
	public ResponseEntity<ErrorResponse> handleError(
		HttpServletRequest request, HttpServerErrorException ex)
	{
		ErrorResponse error = new ErrorResponse();
		error.setMessage(ex.getLocalizedMessage());
		error.setErrorCode(HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	/**
	 * Handles {@link ResourceAccessException} exceptions.
	 * 
	 * @param request ex the failing request
	 * @param ex instance of {@link ResourceAccessException}
	 * @return the response with the HTTPStatus GATEWAY_TIMEOUT and the
	 * corresponding HTTPBody (The message from the thrown exception)
	 */
	@ExceptionHandler(ResourceAccessException.class)
	public ResponseEntity<ErrorResponse> handleError(
		HttpServletRequest request, ResourceAccessException ex)
	{
		ErrorResponse error = new ErrorResponse();
		error.setMessage("Internal server error !");
		error.setErrorCode(HttpStatus.GATEWAY_TIMEOUT.value());
		return new ResponseEntity<>(error, HttpStatus.GATEWAY_TIMEOUT);
	}

	/**
	 * Handles {@link ConnectException} exceptions.
	 *
	 * @param request the failing request
	 * @param ex instance of {@link ConnectException}
	 * @return the response with the HTTPStatus GATEWAY_TIMEOUT
	 */
	@ExceptionHandler(ConnectException.class)
	public ResponseEntity<ErrorResponse> handleError(
		HttpServletRequest request, java.net.ConnectException ex)
	{
		ErrorResponse error = new ErrorResponse();
		error.setMessage(ex.getMessage());
		error.setErrorCode(HttpStatus.GATEWAY_TIMEOUT.value());
		return new ResponseEntity<>(error, HttpStatus.GATEWAY_TIMEOUT);
	}
}
