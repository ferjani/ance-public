package com.ngtech.ance.mpki.ws.repositories;

import com.ngtech.ance.mpki.ws.entities.Administrator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * This repository allows to manage RaAdministrator entities.
 *
 * @author Khadija Ferjani
 */
@Repository
public interface AdministratorRepository
	extends PagingAndSortingRepository<Administrator, Integer>,
	JpaSpecificationExecutor<Administrator>
{

	/**
	 * Fetches the RA admin with the given dn.
	 *
	 * @param dn the RA admin dn
	 * @return the RA admin with the given dn or {@code null}
	 */
	public Administrator findByDn(@Param("dn") String dn);

	/**
	 * Fetches the RA admin with the given email.
	 *
	 * @param email the RA admin email
	 * @return the RA admin with the given email or {@code null}
	 */
	public Administrator findByEmail(@Param("email") String email);

	/**
	 * Fetches the RA admin with the given email and status.
	 *
	 * @param email the RA admin email
	 * @param status the user status to be used for filtering
	 * @return the RA admin with the given email or {@code null}
	 */
	public Administrator findByEmailAndStatus(@Param("email") String email,
		@Param("status") String status);

	/**
	 * Fetches the RA operator with the given email, phone number and status.
	 *
	 * @param phoneNumber the RA operator phoneNumber
	 * @param email the RA operator email
	 * @param status the user status
	 * @return the RA operator with the given email and phone number or {@code null}
	 */
	public Administrator findByEmailAndPhoneNumberAndStatus(@Param("email") String email,
		@Param("phone_number") String phoneNumber, @Param("status") String status);

	/**
	 * Fetches the RA operator with the given email and phone number. The user must not have the
	 * given status.
	 *
	 * @param phoneNumber the RA operator phoneNumber
	 * @param email the RA operator email
	 * @param status the user status
	 * @return the RA operator with the given email and phone number or {@code null}
	 */
	public Administrator findByEmailAndPhoneNumberAndStatusNot(@Param("email") String email,
		@Param("phone_number") String phoneNumber, @Param("status") String status);

	/**
	 * Gets all operators. This method uses the paging process defined by spring.
	 *
	 * @param status the user status
	 * @param pageable the page of Operators.
	 * @return list of Administrator.
	 */
	public Page<Administrator> findByStatusNot(
		@Param("status") String status, Pageable pageable);

}
