package com.ngtech.ance.mpki.ws.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This entity bean represents an RA operator informations stored in database.
 *
 * @author Abir Ghoudi
 * @author Khadija Ferjani
 */
@Entity
@Table(name = "administrator")
@NamedQueries({
	@NamedQuery(name = "Administrator.findAll", query = "SELECT a FROM Administrator a"),
	@NamedQuery(name = "Administrator.findById", query
		= "SELECT a FROM Administrator a WHERE a.id = :id"),
	@NamedQuery(name = "Administrator.findByCreationDate", query
		= "SELECT a FROM Administrator a WHERE a.creationDate = :creationDate"),
	@NamedQuery(name = "Administrator.findByDeletionDate", query
		= "SELECT a FROM Administrator a WHERE a.deletionDate = :deletionDate"),
	@NamedQuery(name = "Administrator.findByEmail", query
		= "SELECT a FROM Administrator a WHERE a.email = :email"),
	@NamedQuery(name = "Administrator.findByFirstName", query
		= "SELECT a FROM Administrator a WHERE a.firstName = :firstName"),
	@NamedQuery(name = "Administrator.findByLastName", query
		= "SELECT a FROM Administrator a WHERE a.lastName = :lastName"),
	@NamedQuery(name = "Administrator.findByPhoneNumber", query
		= "SELECT a FROM Administrator a WHERE a.phoneNumber = :phoneNumber"),
	@NamedQuery(name = "Administrator.findByRole", query
		= "SELECT a FROM Administrator a WHERE a.role = :role"),
	@NamedQuery(name = "Administrator.findByDn", query
		= "SELECT a FROM Administrator a WHERE a.dn = :dn"),
	@NamedQuery(name = "Administrator.findByStatus", query
		= "SELECT a FROM Administrator a WHERE a.status = :status"),
	@NamedQuery(name = "Administrator.findByUuid", query
		= "SELECT a FROM Administrator a WHERE a.uuid = :uuid")})
public class Administrator implements Serializable
{

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "id")
	private Integer id;

	@Basic(optional = false)
	@Column(name = "dn")
	private String dn;

	@Basic(optional = false)
	@Column(name = "creation_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Column(name = "deletion_date")
	@Temporal(TemporalType.DATE)
	private Date deletionDate;

	@Basic(optional = false)
	@Column(name = "email")
	private String email;

	@Basic(optional = false)
	@Column(name = "first_name")
	private String firstName;

	@Basic(optional = false)
	@Column(name = "last_name")
	private String lastName;

	@Basic(optional = false)
	@Column(name = "phone_number")
	private String phoneNumber;

	@Basic(optional = false)
	@Column(name = "role")
	private String role;

	@Basic(optional = false)
	@Column(name = "status")
	private String status;

	@Basic(optional = false)
	@Column(name = "uuid")
	private String uuid;


	/**
	 * The RaAdministrator unique identifier.
	 *
	 * @return the RaAdministrator identifier
	 */
	public Integer getId()
	{
		return id;
	}

	/**
	 * The RaAdministrator unique identifier.
	 *
	 * @param id the RaAdministrator identifier
	 */
	public void setId(Integer id)
	{
		this.id = id;
	}

	/**
	 * Gets the administrator first name.
	 *
	 * @return the administrator's first name
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * Sets the administrator first name.
	 *
	 * @param firstName the administrator's first name
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * Gets the administrator last name.
	 *
	 * @return the administrator's last name
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * Sets the administrator last name.
	 *
	 * @param lastName the administrator's last name
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * Returns the administrator phone number.
	 *
	 * @return the administrator phone number
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * Sets the administrator phone number.
	 *
	 * @param phoneNumber the administrator phone number
	 */
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Returns the creation date of the operator.
	 *
	 * @return the creation date of the operator
	 */
	public Date getCreationDate()
	{
		return creationDate;
	}

	/**
	 * Sets the creation date of the operator.
	 *
	 * @param creationDate the creation date of the operator.
	 */
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}

	/**
	 * Returns the deletion date of the operator.
	 *
	 * @return the deletion date of the operator
	 */
	public Date getDeletionDate()
	{
		return deletionDate;
	}

	/**
	 * Sets the deletion date of the operator.
	 *
	 * @param deletionDate the deletion date of the operator
	 */
	public void setDeletionDate(Date deletionDate)
	{
		this.deletionDate = deletionDate;
	}

	/**
	 * Returns the administrator's DN.
	 *
	 * @return the certificate DN
	 */
	public String getDN()
	{
		return dn;
	}

	/**
	 * Sets the administrator's DN.
	 *
	 * @param dn the certificate DN
	 */
	public void setDN(String dn)
	{
		this.dn = dn;
	}

	/**
	 * Returns the admin email.
	 *
	 * @return the admin email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * Sets the admin email.
	 *
	 * @param email the admin email
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * Returns the admin role.
	 *
	 * @return the admin role
	 */
	public String getRole()
	{
		return role;
	}

	/**
	 * Sets the admin role.
	 *
	 * @param role the admin role
	 */
	public void setRole(String role)
	{
		this.role = role;
	}

	/**
	 * Returns the operator status.
	 *
	 * @return the operator status.
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * Sets the operator status.
	 *
	 * @param status the operator status.
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}

	/**
	 * Returns the admin unique identifier.
	 *
	 * @return the operator unique identifier
	 */
	public String getUuid()
	{
		return uuid;
	}

	/**
	 * Sets the admin unique identifier.
	 *
	 * @param uuid the operator unique identifier
	 */
	public void setUuid(String uuid)
	{
		this.uuid = uuid;
	}

}
