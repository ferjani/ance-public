package com.ngtech.ance.mpki.ws.security;

import com.ngtech.ance.mpki.ws.entities.Administrator;
import com.ngtech.ance.mpki.ws.services.AdministratorService;

import java.security.cert.X509Certificate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.x509.SubjectDnX509PrincipalExtractor;

/**
 * Enable to configure mutual authentication.
 *
 * @author Khadija FERJANI
 */
@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class X509AuthenticationServer extends WebSecurityConfigurerAdapter
{

	private final AdministratorService service;

	/**
	 * Creates a {@code X509AuthenticationServer}.
	 * 
	 * @param service the admin service
	 */
	public X509AuthenticationServer(AdministratorService service)
	{
		this.service = service;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http.authorizeRequests().anyRequest().authenticated().and()
			.x509().x509PrincipalExtractor(new MySubjectDnX509PrincipalExtractor())
			.userDetailsService(userDetailsService());

	}

	@Bean
	@Override
	public UserDetailsService userDetailsService()
	{
		return new UserDetailsService()
		{
			@Override
			public UserDetails loadUserByUsername(String username)
			{
				Administrator admin = service.findByDn(username);
				if (admin == null) {
					throw new UsernameNotFoundException("The user was not "
						+ "found: " + username);
				}

				return new User(admin.getFirstName() + " " + admin.getLastName(),
					"", AuthorityUtils.commaSeparatedStringToAuthorityList(
						admin.getRole()));
			}
		};
	}

	/**
	 * This class is used to return the whole DN from the certificate.
	 */
	public class MySubjectDnX509PrincipalExtractor extends SubjectDnX509PrincipalExtractor
	{

		@Override
		public Object extractPrincipal(X509Certificate clientCert)
		{
			return clientCert.getSubjectDN().getName();

		}
	}
}
