package com.ngtech.ance.mpki.ws.services;

import com.ngtech.ance.mpki.ws.entities.Administrator;
import com.ngtech.ance.mpki.ws.repositories.AdministratorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This service is used to manipulate the Ra administrator's info.
 *
 * @author Khadija Ferjani
 */
@Service
public class AdministratorService
{
	private final AdministratorRepository adminRepo;

	/**
	 * Creates a new {@code RaAdministratorService} object.
	 *
	 * @param adminRepo the Ra administrator repository
	 */
	@Autowired
	public AdministratorService(AdministratorRepository adminRepo)
	{
		this.adminRepo = adminRepo;
	}
	
	/**
	 * Finds the admin by his dn.
	 *
	 * @param cn the adminRepo DN
	 * @return the admin or {@code null}
	 */
	public Administrator findByDn(String cn)
	{
		return adminRepo.findByDn(cn);
	}

	/**
	 * Finds the admin by his email.
	 *
	 * @param email the adminRepo email
	 * @return the adminRepo or {@code null}
	 */
	public Administrator findByEmail(String email)
	{
		return adminRepo.findByEmail(email);
	}

	/**
	 * Saves the given operator.
	 *
	 * @param operator the operator to save
	 * @return the saved operator
	 */
	public Administrator saveOperator(Administrator operator)
	{
		return adminRepo.save(operator);
	}
}

