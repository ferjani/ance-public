package com.ngtech.ance.mpki.ws.controllers;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class allows to test the user authentication.
 * 
 */
@RestController
@RequestMapping("/testing")
public class TestingController
{

	private static final Logger LOGGER = LoggerFactory.getLogger(TestingController.class);

	/**
	 * AuthenticationController's constructor.
	 */
	@Autowired
	public TestingController()
	{

	}

	/**
	 * Test web service.
	 *
	 * @param principal the authenticated user
	 * @return a null string
	 */
	@PreAuthorize("hasAuthority('ROLE_USER')")
	@RequestMapping(value = "/test")
	public String user(Principal principal)
	{
		return "success";
	}
}
